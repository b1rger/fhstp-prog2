#include <stdio.h>
#include <stdint.h>

#define OCTMAX 255
#define OCTMIN 0
#define LOCALHOST "127.0.0.1"
#define SUCCESS 1
#define FAILURE 0

uint8_t isIp(const char *ip);
uint8_t isOct(int *i);
void printoutput(const char *ip);

int main(int argc, char **argv)
{
	const char *strings[] = {
		LOCALHOST,
		"128.130.179.38",
		"192.168.0.1",
		"255.255.255.0",
		"127.zero.0.1",
		"127.2555.0.1",
		"a.b.c.d",
		"foobar.barfaz.0.1",
		"0.0.0.0"
	};
	for (uint8_t i = 0; i < sizeof(strings)/sizeof(strings[0]); i++) {
		printoutput(strings[i]);
	}
}

void printoutput(const char *ip) {
	printf("%s", ip);
	if (isIp(ip)) {
		printf(" is a valid IP address\n");
	} else {
		printf(" is not a valid IP address\n");
	}
}

uint8_t isIp(const char *ip)
{
	int oc0, oc1, oc2, oc3;
	if (sscanf(ip, "%d.%d.%d.%d", &oc0, &oc1, &oc2, &oc3) == 4)
		if (isOct(&oc0) && isOct(&oc1) && isOct(&oc2) && isOct(&oc3))
			return SUCCESS;
	return FAILURE;
}

uint8_t isOct(int *i) {
	if ((*i >= OCTMIN) && (*i <= OCTMAX))
		return SUCCESS;
	return FAILURE;
}
