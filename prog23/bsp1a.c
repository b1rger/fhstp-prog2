#include <stdio.h>
#include <stdint.h>

void my_pow(signed int const base, unsigned int const exponent, long *result);

int main(int argc, char **argv)
{
	int base, exponent, done = 0;
	long result;
	while (!done) {
		printf("Eingabe von Base und Exp (z.B. 3^4): ");
		if (scanf("%d^%d", &base, &exponent) != 2) {
			done = 1;
		}
		my_pow(base, exponent, &result);
		printf("--> %ld\n", result);
	}

}

void my_pow(signed int const base, unsigned int const exponent, long *result)
{
	*result = base;
	for (uint8_t i = 1; i < exponent; i++) {
		*result *= base;
	}
}
