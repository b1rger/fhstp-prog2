#include <stdio.h>

signed char my_abs(signed int *x);

int main(int argc, char **argv)
{
	int x, done = 0;
	while (!done) {
		printf("Zahl eingeben: ");
		if (scanf("%d", &x) != 1) {
			done = 1;
		} else {
			printf("--> %d (%d)\n", x, my_abs(&x));
		}
	}
}

signed char my_abs(signed int *x)
{
	if (*x < 0) {
		*x *= -1;
		return -1;
	} else {
		return 1;
	}
}
