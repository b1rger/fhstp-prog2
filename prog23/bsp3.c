#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef enum { false, true } bool;

void bubble_sort(int *array, size_t n);

int main(int argc, char **argv)
{
	unsigned int seed, length;
	printf("Geben Sie eine Zahl fuer den Seedwert ein: ");
	scanf("%d", &seed);
	printf("Geben Sie die Anzahl der Elemente an: ");
	scanf("%d", &length);
	
	int array[length];
	srand(seed);
	printf("unsortiert:\n ");
	for (unsigned int i = 0; i < length; i++) {
		array[i] = rand() % 100;
		printf("%2d ", array[i]);
	}
	printf("\n");
	bubble_sort(array, sizeof(array)/sizeof(array[0]));
}

void bubble_sort(int *array, size_t n)
{
	bool swapped = true;
	unsigned int iteration = 0;

	while (swapped) {
		unsigned int swapcount = 0;
		swapped = false;
		for (unsigned int i = 0; i < n-1; i++) {
			if (array[i] > array[i+1]) {
				int tmp = array[i];
				array[i] = array[i+1];
				array[i+1] = tmp;
				swapcount++;
				swapped = true;
			}
		}
		iteration++;

		printf("round %d, swap count: %d\n ", iteration, swapcount);
		for (unsigned int i = 0; i < n; i++) {
			printf("%2d ", array[i]);
		}
		printf("\n");
	}
	printf("finish at round %d of max %ld\n", iteration, n);
}
