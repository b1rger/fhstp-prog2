/*
 * Copyright (C) 2018 Birger Schacht <is161323@fhstp.ac.at>
 * License: GPLv3 (see https://spdx.org/licenses/GPL-3.0-or-later.html for details)
 *
 * Assignment: write a custom function which will analyze the words in the text
 * according to specific criteria
 */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdint.h>

#define SPACE 32
#define NEWLINE 10

// define a struct that stores our metadata
struct metadata {
	int wordNumber;
	int totalVowels;
	int totalConsonants;
	int totalDigits;
	int totalOthers;
};

int parsechar(char c);
void parseword(char *word, unsigned int *vow, unsigned int *con, unsigned int *dig, unsigned int *spe);
void analyseWord(char *line);

int main(int argc, char **argv)
{
	char line[BUFSIZ];
	char linewonewline[BUFSIZ];
	// read input, print the line and concatenate the line to `linewenewline`
	while(fgets(line, BUFSIZ, stdin) != NULL) {
		printf("%s", line);
		strcat(linewonewline, line);
	}
	// replace all newlines in `linewenewline` with spaces
	for (unsigned int i = 0; i < strlen(linewonewline); i++) {
		if (linewonewline[i] == NEWLINE) {
			linewonewline[i] = SPACE;
		}
	}
	// parse the line and store the result in the `textdata` struct
	analyseWord(linewonewline);
}

/*
 * split a line up in words and pass every word to the `parseword` method
 */
void analyseWord(char *line) {
	struct metadata textdata;
	textdata.totalOthers = textdata.totalDigits = textdata.totalConsonants = textdata.totalVowels = 0;
	uint8_t begin = 0, end = 0;
	printf("VOWELS;CONSONANTS;DIGITS;OTHERS;WORD\n");
	for (unsigned int i = 0; i < strlen(line); i++) {
		if (line[i] == SPACE) {
			end = i;
		}
		if (end > begin) {
			unsigned int vow = 0, con = 0, dig = 0, oth = 0; 
			char tmpword[end - begin];
			strncpy(tmpword, line+begin, end-begin);
			tmpword[end-begin] = '\0';
			parseword(tmpword, &vow, &con, &dig, &oth);
			textdata.wordNumber++;
			textdata.totalVowels+=vow;
			textdata.totalDigits+=dig;
			textdata.totalConsonants+=con;
			textdata.totalOthers+=oth;
			printf("%i\t|%i\t|%i\t|%i\t|%s\n",vow, con, dig, oth, tmpword);
			begin = end+1;
		}
	}
	printf("Total number of words: %i\n", textdata.wordNumber);
	printf("Total number of Vowels: %i\n", textdata.totalVowels);
	printf("Total number of Consonants: %i\n", textdata.totalConsonants);
	printf("Total number of Digits: %i\n", textdata.totalDigits);
	printf("Total number of other characters: %i\n", textdata.totalOthers);
}

/*
 * parse every word and pass every character to the parsechar method
 * increment the variables accordingly
 */
void parseword(char *word, unsigned int *vow, unsigned int *con, unsigned int *dig, unsigned int *oth)
{
	for (unsigned int i = 0; i < strlen(word); i++) {
		switch (parsechar(word[i])) {
			case 0:
			(*dig)++;
			break;
			case 1:
			(*oth)++;
			break;
			case 2:
			(*vow)++;
			break;
			case 3:
			(*con)++;
			break;
		}
	}
}

/* parse on character */
int parsechar(char c)
{
	if (isdigit(c))
		return 0;
	if (!isalpha(c)) {
		return 1;
	}
	switch(tolower(c)) {
		case 'a': case 'A':
		case 'e': case 'E':
		case 'i': case 'I':
		case 'o': case 'O':
		case 'u': case 'U':
		return 2;
	}
	return 3;
}
