/*
 * Copyright (C) 2018 Birger Schacht <is161323@fhstp.ac.at>
 * License: GPLv3 (see https://spdx.org/licenses/GPL-3.0-or-later.html for details)
 *
 * Assignment: In this exercise, you need to write a program which will compute
 * the price for a house that you are trying to resell
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#define RAIIM 2.5f
#define ERSIM 2.4f
#define AUSIM 2.73f

// create a struct for the house and its characteristics
struct house {
    char name[BUFSIZ];
    int width;
    int length;
    float pricePerM2;
    float totalPrice;
};

float getPricePerM2();
void finalPrice(struct house*s1);

int main(int argc, char **argv) {
	// define a new house and set its name, width and length
    struct house foobar;
    strcpy(foobar.name, "Foobar");
    foobar.width = 5;
    foobar.length = 7;

	// set the price per m^2, according to the immo company
    foobar.pricePerM2 = getPricePerM2();
    printf("Set price per m2 to %f\n", foobar.pricePerM2);
	// pass a pointer to the house struct to the `finalPrice`
	// method
    finalPrice(&foobar);
    printf("The house '%s' of size %ux%u costs %f euros.\n", foobar.name, foobar.width, foobar.length, foobar.totalPrice);
}

/*
 * get input from user and return the corresponding price per m^2
 */
float getPricePerM2() {
    int immo;
    printf("For this house you have 3 available sellers.\n");
    printf("1) Raifeisen Immo\n2) Erste Immo\n3) Austria Immo\n");
    printf("Please choose one: ");
    scanf("%i", &immo);

    switch (immo) {
        case 1:
            return RAIIM;
        case 2:
            return ERSIM;
        case 3:
            return AUSIM;
    }
    return 0;
}

/*
 * calculate the final price
 */
void finalPrice(struct house*s1) {
    s1->totalPrice = (s1->width * s1->length) * s1->pricePerM2;
}
