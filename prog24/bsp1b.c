/*
 * Copyright (C) 2018 Birger Schacht <is161323@fhstp.ac.at>
 * License: GPLv3 (see https://spdx.org/licenses/GPL-3.0-or-later.html for details)
 *
 * Assignment: Write a custom function which will be able to extract specific
 * characters from a string.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * everySecondChar(char * dest, char * input);

int main(int argc, char **argv)
{
    char line[BUFSIZ];
	// dest will be the char array that contains the result
    char *dest = malloc(1*sizeof(char));

    printf("Inputted text:\n");
	// read a line and pass the line and the destination
	// array to everySecondChar
    while(fgets(line,BUFSIZ,stdin) != NULL) {
        printf("%s", line);
        dest = everySecondChar(dest, line);
    }
    printf("\nResulting string:\n%s\n", dest);

}

// iterate thrugh input character array 2 items at a time
// if item is NaN, make the destination array one character
// longer and add the item
char * everySecondChar(char * dest, char * input) {
    static unsigned int e = 0;
    for (unsigned int i = 0; i <= strlen(input)-1; i+=2) {
        if ((input[i] < 48) || (input[i] > 57)) {
            dest = realloc(dest, (e+1)*sizeof(char));
            dest[e] = input[i];
            e++;
            //printf("%s is now %zu big, e is %i\n", dest, strlen(dest), e);
        }
    }
    return dest;
}
