/*
 * Copyright (C) 2018 Birger Schacht <is161323@fhstp.ac.at>
 * License: GPLv3 (see https://spdx.org/licenses/GPL-3.0-or-later.html for details)
 *
 * Assignment: Write a program which would be able to translate English words
 * into German
 */
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#define MAX_WORDS 256

int main(int argc, char **argv)
{
	// define a dictionay array and fill it with words
    char *dictionary[MAX_WORDS][2];
    dictionary[0][0] = "Apple";
    dictionary[0][1] = "Apfel";
    dictionary[1][0] = "Plane";
    dictionary[1][1] = "Flugzeug";
    dictionary[2][0] = "yes";
    dictionary[2][1] = "ja";
    char word[BUFSIZ], enword[BUFSIZ];
    uint8_t notrans = 0;

	// read the english word and iterate through the dictionary
	// array; if the dictionary does not contain the word, set
	// `notrans`, so we can print an explanation. otherwise, store
	// the german word in the `endword` variable
    printf("Enter the english word that you want to translate: ");
    scanf("%s", word);
    for (unsigned int i = 0; i < sizeof(dictionary)/sizeof(dictionary[0]); i++) {
        if (dictionary[i][0] == 0) {
            notrans = 1;
            break;
        }
        if (strcmp(dictionary[i][0], word) == 0) {
            strcpy(enword,dictionary[i][1]);
            break;
        }
    }

    if (notrans) {
        printf("There is no translation for %s\n", word);
    } else {
        printf("The german translation of '%s' is '%s'\n", word, enword);
    }
    


}
