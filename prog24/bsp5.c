/*
 * Copyright (C) 2018 Birger Schacht <is161323@fhstp.ac.at>
 * License: GPLv3 (see https://spdx.org/licenses/GPL-3.0-or-later.html for details)
 *
 * Assignment: Write a program that performs particular operations over set of
 * matrices.
 */
#include <stdio.h>

void addition (int rows, int columns, int a[rows][columns], int b[rows][columns], int result[rows][columns]);
void substraction (int rows, int columns, int a[rows][columns], int b[rows][columns], int result[rows][columns]);
void multiplication (int rows, int columns, int a[rows][columns], int b[rows][columns], int result[rows][columns]);
void transposition (int rows, int columns, int a[rows][columns], int result[rows][columns]);
void print(int rows, int columns, int a[rows][columns]);

int main(int argc, char **argv)
{
	int a[3][3] = {{1,2,3},{4,5,6},{7,8,9}};
	int b[3][3] = {{1,2,3},{4,5,6},{7,9,8}};
	int width = sizeof(a)/sizeof(a[0]);
	int length = sizeof(a[0])/sizeof(a[0][0]);
	int result[width][length];

	addition(width, length, a, b, result);
	print(width, length, result);

	substraction(width, length, a, b, result);
	print(width, length, result);

	multiplication(width, length, a, b, result);
	print(width, length, result);

	transposition(width, length, a, result);
	print(width, length, result);

}

/*
 * add matrices
 */
void addition (int rows, int columns, int a[rows][columns], int b[rows][columns], int result[rows][columns])
{
	for (int i=0; i<rows; i++) {
		for (int e=0; e<columns; e++) {
			result[i][e] = a[i][e] + b[i][e];
		}
	}
}

/*
 * substract matrices
 */
void substraction (int rows, int columns, int a[rows][columns], int b[rows][columns], int result[rows][columns])
{
	for (int i=0; i<rows; i++) {
		for (int e=0; e<columns; e++) {
			result[i][e] = a[i][e] - b[i][e];
		}
	}
}

/*
 * multiply matrices
 */
void multiplication (int rows, int columns, int a[rows][columns], int b[rows][columns], int result[rows][columns])
{
	int sum = 0;
	for (int i=0; i<rows; i++) {
		for (int e=0; e<columns; e++) {
			sum = 0;
			for (int j=0; j<rows; j++) {
				sum += a[i][j] * b[j][e];
			}
			result[i][e] = sum;
		}
	}
}

/*
 * tranpose matrix
 */
void transposition (int rows, int columns, int a[rows][columns], int result[rows][columns])
{
	for (int i=0; i<rows; i++) {
		for (int e=0; e<columns; e++) {
			result[i][e] = a[e][i];
		}
	}
}

/*
 * print matrix
 */
void print(int rows, int columns, int a[rows][columns])
{
	for (int i=0; i<columns; i++) {
		printf("-----\t");
	}
	printf("\n");
	for (int i=0; i<rows; i++) {
		for (int e=0; e<columns; e++) {
			printf("[%3i]\t", a[i][e]);
		}
		printf("\n");
	}
}
