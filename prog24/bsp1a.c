/*
 * Copyright (C) 2018 Birger Schacht <is161323@fhstp.ac.at>
 * License: GPLv3 (see https://spdx.org/licenses/GPL-3.0-or-later.html for details)
 *
 * Assignment: write custom function which will be able to compare specific
 * parts of the strings
 */

#include <stdio.h>
#include <string.h>

#define STRING_MAX 256

char* my_compare(const char * str1, const char * str2, int index_start, int index_end);

int main(int argc, char **argv)
{
    char str1[STRING_MAX], str2[STRING_MAX];
    int index_start, index_end;

    printf("Enter first string: ");
    fgets(str1, STRING_MAX, stdin);
    printf("Enter second string: ");
    fgets(str2, STRING_MAX, stdin);
    printf("Enter the start index: ");
    scanf("%d", &index_start);

	// check if the start index is in the permitted range
    if ((index_start > strlen(str1)) || (index_start > strlen(str2)) || (index_start < 0)) {
        return 1;
    }

    printf("Enter end index: ");
    scanf("%d", &index_end);
	// check if the end index is in the permitted range
    if ((index_end > strlen(str1)) || (index_end > strlen(str2)) || (index_end < 0)) {
        return 1;
    }

    printf("The final match is: %s\n", my_compare(str1, str2, index_start, index_end));

}

/* compare the two strings */
char* my_compare(const char * str1, const char * str2, int index_start, int index_end)
{
	// create a char array for the result
    static char match[STRING_MAX];
    for (int i = index_start; i <= index_end; i++) {
        if (str1[i] == str2[i]) {
            match[i-index_start] = str1[i];
        } else {
            // 32 is ascii for the space character
            match[i-index_start] = 32;
        }
    }
    return match;
}
