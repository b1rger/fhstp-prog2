#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define MAX_NAMELEN 20
#define FILENAME "carlist.bin"

typedef struct car
{
    char name[MAX_NAMELEN];
    int year;
    double fuel;
} car_t;

int car_insert(FILE *fp, const car_t* car);
void car_print_all(FILE *fp);
int car_remove(FILE *fp, unsigned int index);
car_t *getcar();
int getindex();

int main(int argc, char **argv)
{
    FILE *fp;

	// open the file; if this fails
	// create the file in append mode
    fp = fopen(FILENAME, "r+b");
	if (!fp)
		fp = fopen(FILENAME, "a+b");

	// print all the cars and then await further
	// instructions from the user
    car_print_all(fp);
    printf("<D>elete or <C>reate? ");
    switch(tolower(fgetc(stdin))) {
    case 'c':
        car_insert(fp, getcar());
        break;
    case 'd':
        car_remove(fp, getindex());
        break;
    }

    fclose(fp);
}

// print all the cars!
// first we find out how big the file is, then we
// divide this by the size of the car struct to get
// the number of cars; then we iterate through them
// and print them if they don't have a '-' char as
// first character of the name
void car_print_all(FILE *fp)
{
    printf("Print all cars\n");
   	fseek(fp, 0, SEEK_END);
    int number = ftell(fp)/sizeof(struct car);
    car_t cars[number];
    rewind(fp);
    int numberofcars = fread(cars, sizeof(car_t), number, fp);
    car_t *iterator = cars;

    for (int i = 0; i < numberofcars; i++, iterator++) {
        if (iterator->name[0] != '-')
            printf("car\t%d: %s %d %lf\n", i+1, iterator->name, iterator->year, iterator->fuel);
    }
}

// get user input about the car to be included
car_t *getcar()
{
    static car_t car;
    printf("Name of the car: ");
    scanf("%s", car.name);
    printf("Year of manufacture: ");
    scanf("%d", &car.year);
    printf("Average of consumption: ");
    scanf("%lf", &car.fuel);

    return &car;
}

// get the index of the car to be deleted
int getindex() {
    int index;
    printf("Enter index of car to be deleted: ");
    scanf("%d", &index);
    return index;
}

// insert a car in the file;
// first we find out how big the file is, then we
// divide this by the size of the car struct to get
// the number of cars; then we create an array thats
// on car bigger and read the file content into this
// array. then we set the last item in the array to
// be the new car, rewind the filestream back to the
// beginning and write the array back to the file
int car_insert(FILE *fp, const car_t* car) {
	/*struct car carray[2] = {{"audi", 1999, 1.2},{"volvo", 2000, 1.1}};
	fwrite (&carray, sizeof(struct car), 2, fp);
	*/
    fseek(fp, 0, SEEK_END);
    int number = ftell(fp)/sizeof(struct car);
    car_t cars[number+1];
    rewind(fp);
    int numberofcars = fread(&cars, sizeof(struct car), number, fp);
    cars[numberofcars] = *car;
    rewind(fp);
    if (fwrite(&cars, sizeof(struct car), numberofcars+1, fp) == numberofcars+1)
        return 1;
    return 0;
}

// mark a car as deleted in the file;
// first we find out how big the file is, then we
// divide this by the size of the car struct to get
// the number of cars; then we create an array that
// gets filled with the content of the file; the name
// of the car at the `index` of the array will be
// prepended with a '-' character
int car_remove(FILE *fp, unsigned int index) {
    rewind(fp);
    fseek(fp, 0, SEEK_END);
    int number = ftell(fp)/sizeof(struct car);
    car_t cars[number];
    rewind(fp);
    int numberofcars = fread(&cars, sizeof(struct car), number, fp);
    char name[MAX_NAMELEN] = "-";
    strcpy(cars[index-1].name, strcat(name, cars[index-1].name));
    rewind(fp);
    if (fwrite(&cars, sizeof(struct car), numberofcars, fp) == numberofcars)
        return 1;
    return 0;
}
