#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#define MAX_NAMELEN 20
#define FILENAME "carlist.lst"

typedef struct car
{
    char name[MAX_NAMELEN];
    int year;
    double fuel;
} car_t;

int car_insert(FILE *fp, const car_t* car);
void car_print_all(FILE *fp);
int car_remove(FILE *fp, unsigned int index);
car_t *getcar();
int getindex();

int main(int argc, char **argv)
{
    FILE *fp;

	// try to open the file, if this fails
	// create a new file in append mode
    fp = fopen(FILENAME, "r+");
	if (!fp)
		fp = fopen(FILENAME, "a+");

	// print all the cars and aks for
	// further instructions
	if (fp) {
		car_print_all(fp);
		printf("<D>elete or <C>reate? ");
		switch(tolower(fgetc(stdin))) {
		case 'c':
			car_insert(fp, getcar());
			break;
		case 'd':
			car_remove(fp, getindex());
			break;
		}

		fclose(fp);
	} else {
		return EXIT_FAILURE;
	}
}

// go through the file line by line
// parse every line and store the comma seperated values
// in the variables and then output them
void car_print_all(FILE *fp)
{
    char line[BUFSIZ];
    int i = 1;
    while(fgets(line, BUFSIZ, fp) != NULL) {
        char name[MAX_NAMELEN];
        int year;
        double fuel;
        sscanf(line, " %[^,],%d,%lf", name, &year, &fuel);
        if (name[0] != '-')
            printf("car\t%d: %s %d %lf\n", i, name, year, fuel);
        i++;
    }
}

// get input from the user about the car and return
// the info packed into a car struct
car_t *getcar()
{
    static car_t car;
    printf("Name of the car: ");
    scanf("%s", car.name);
    printf("Year of manufacture: ");
    scanf("%d", &car.year);
    printf("Average of consumption: ");
    scanf("%lf", &car.fuel);

    return &car;
}

// get the index of the car that should be deleted
int getindex() {
    int index;
    printf("Enter index of car to be deleted: ");
    scanf("%d", &index);
    return index;
}

// append a car as a line at the end of the file
int car_insert(FILE *fp, const car_t* car) {
    return fprintf(fp, " %s,%d,%lf\n", car->name, car->year, car->fuel);
}

// remove a car from the list: if the index is 1 we remove the first line by
// putting a '-' character as the first char in the file; 
// otherwise we'll go through the file line by line and increment index
// accordingly; if we find the line to be removed we prepend it with a '-'
// character
int car_remove(FILE *fp, unsigned int index) {
    rewind(fp);
    if (index == 1) {
        fputs("-", fp);
    } else {
        int temp = 1;
        char line[BUFSIZ];
        --index;
        while (fgets(line, BUFSIZ, fp) != NULL) {
            if (temp != index) {
                temp++;
            } else {
                fputs("-", fp);
                break;
            }
        }
    }
    return 1;
}
