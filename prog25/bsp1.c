#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>


struct dice {
	unsigned int sides;
	unsigned int val_cur;
};

void rollDice(struct dice *di);
void printDice(struct dice *di, int number);

int main(int argc, char **argv)
{
	int type = 0;
	int number = 0;

	// check for cli arguments
	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-t") == 0) {
			sscanf(argv[++i], "%d", &type);
		}
		if (strcmp(argv[i], "-n") == 0) {
			sscanf(argv[++i], "%d", &number);
		}
	}


	// print the type of dice and get the input
	// from user if it was not given per argument
	printf("Type of dice (E.g.: D6,D20): ");
	if (type == 0) {
		scanf("%d", &type);
	} else {
		printf("%i\n", type);
	}
		
	// print the number of dices and get the input
	// from user if it was not given per argument
	printf("Number of dice(s): ");
	if (number == 0) {
		scanf("%d", &number);
	} else {
		printf("%i\n", number);
	}
	if (number < 0 || type < 0)
		return EXIT_FAILURE;


	// initialize rng
	srand(time(NULL));

	// allocate the memory for the dices
	struct dice *di, *iterator;
	di = malloc(number*sizeof(struct dice));

	// check if memory allocation was succesful
	if (!di) {
		return -1;
	}

	// for every dice in the array, set the type
	// and roll the dice and then print the dice
	iterator = di;
	for (int i = 0; i < number; i++, iterator++) {
		iterator->sides = type;
		//rollDice(iterator);
		//printDice(iterator, i);
	}
	iterator = di;
	for (int i = 0; i < number; i++, iterator++) {
		rollDice(iterator);
	}
	iterator = di;
	for (int i = 0; i < number; i++, iterator++) {
		printDice(iterator, i);
	}

	// free the memory
	free(di);
	return 0;
}

// roll the dice
void rollDice(struct dice *di)
{
		di->val_cur = (rand() % di->sides) + 1;
}

// print the dice
void printDice(struct dice *di, int number)
{
	printf("Dice %03i --> D%i Value: %02i\n", number+1, di->sides, di->val_cur);
}
