#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>

int main(int argc, char** argv) {
	char c;
	printf("Eingabe: ");
	c = getchar();

	srand(time(NULL));
	char r = rand() % 100;

	//char i = c - '0';
	c -= '0';

	float erg = r + 27 + (pow(c,2) / 2);
	printf("Die Berechnung von %i + 27 + (%i^2 / 2) ergibt %g\n", r, c, erg);
}
