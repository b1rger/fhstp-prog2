#include <stdio.h>
#include <math.h>

int main(int argc, char** argv) {
	unsigned char c;
	c = getchar();
	printf("%c\n", c);
	printf("%c is ascii code %i\n", c, c);

	int i = c - '0';
	//or int i = c - 48

	float erg = 27 + (pow(i,2) / 3) - pow(i,3);
	printf("Ergebnis: %lf\n", erg);
}
