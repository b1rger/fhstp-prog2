#include <stdio.h>

int main(int argc, char** argv) {
	float wa = 13.33793;
        double wb = 99.23234112, wc = 3123456789;

	double erg = 2 * (wa * wb + wb * wc + wa * wc);

	printf("Die berechnete Oberflaeche mit den Seiten a: %f, b: %f und c: %f betraegt: %f\n", wa, wb, wc, erg);
}
