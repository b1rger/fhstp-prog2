#include <stdio.h>

int main(int argc, char **argv)
{
	printf("Size of char is %zu\n", sizeof(char));
	printf("Size of short is %zu\n", sizeof(short));
	printf("Size of signed int is %zu\n", sizeof(signed int));
	printf("Size of unsigned int is %zu\n", sizeof(unsigned int));
	printf("Size of long is %zu\n", sizeof(long));
	printf("Size of size_t is %zu\n", sizeof(size_t));
	printf("Size of float is %zu\n", sizeof(float));
	printf("Size of double is %zu\n", sizeof(double));
	printf("Size of long double is %zu\n", sizeof(long double));
}

