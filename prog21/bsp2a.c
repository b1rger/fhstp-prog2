#include <stdio.h>

int main(int argc, char **argv) {
	unsigned char wa = 25, wb = 50, wc = 30;

	unsigned short erg = 2 * (wa * wb + wb * wc + wa * wc);

	printf("Die berechnete Oberflaeche mit den Seiten a: %u, b: %u und c: %u betraegt: %u\n", wa, wb, wc, erg);
}
