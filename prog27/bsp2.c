#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXCHAR 500
#define MAXLINE 1000

typedef enum { false, true } bool;
struct node {
	char line[MAXCHAR];
	struct node *next;
};

struct node * bubble_sort(struct node* head, bool desc, int word);
void printarray(struct node* head);
int isbigger(char *first, char *second, bool desc, int word);
int startofword(char *string, int word);
struct node * insert(char *line, struct node* head);
struct node * swap(struct node *a, struct node *b);


int main(int argc, char **argv)
{
    char line[MAXCHAR];
    bool desc = false;
    int word = 0;

	/* parse the command line arguments */
    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-d") == 0)
            desc = true;
        if (strcmp(argv[i], "-n") == 0)
            sscanf(argv[++i], "%d", &word);
    }
	
	struct node* head = NULL;
	head = NULL;
	/* read in the lines and store them in the pointer array*/
    while (fgets(line, MAXCHAR, stdin)) {
		head = insert(line, head);
    }

    printarray(head);
    head = bubble_sort(head, desc, word);
    printarray(head);
	free(head);
}

/* insert a line into the linked list `head` points to */
struct node * insert(char *line, struct node * head)
{
	struct node* new  = (struct node*)malloc(sizeof(struct node));
	strcpy(new->line,line);
	new->next = NULL;
	if (head != NULL) {
		new->next = head;
	} 
	head = new;
	return head;
}

/* print all the lines in the array */
void printarray(struct node * head) {
	struct node *tmp = head;
	while (tmp != NULL) {
		printf("%s", tmp->line);
		tmp = tmp->next;
	}
	printf("-\n");
}

/* sort the lines in linked list */
struct node * bubble_sort(struct node* head, bool desc, int word)
{
	bool swapped = true;
	struct node *top  = (struct node*)malloc(sizeof(struct node));
	struct node *p, *q;
	top->next = head;

	while (swapped) {
		swapped = false;
		q = top;
		p = top->next;
		while (p->next != NULL) {
			if (isbigger(p->line,p->next->line, desc, word)) {
				q->next = swap(p, p->next);
                swapped = true;
			}
			q = p;
			if (p->next != NULL)
				p = p->next;
		}
	}
	p = top->next;
	free(top);
	return p;
}

/* swap two elements in the linked list - 
   this is done by swapping the pointers, not the
   content */
struct node * swap(struct node *a, struct node *b) {
	//printf("swapping %s with %s", a->line, b->line);
	a->next = b->next;
	b->next = a;
	return b;
}

/* tests if the first string is 'bigger' */
int isbigger(char *first, char *second, bool desc, int word) 
{
	int firstbegin = startofword(first, word);
	int secondbegin = startofword(second, word);
    while (first[firstbegin] == second[secondbegin]) {
        ++firstbegin, ++secondbegin;
    }
    if (first[firstbegin] > second[secondbegin])
        return !desc;
    return desc;
}

/* get the start of the first word */
int startofword(char *string, int word) {
	if (word == 0)
		return word;
	for (int i = 0; i < strlen(string); i++) {
		if (string[i] == ' ')
			--word;
		if (word == 1)
			return i+1;
	}
	return strlen(string);
}
