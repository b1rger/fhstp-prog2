#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXCHAR 500
#define MAXLINE 1000

typedef enum { false, true } bool;

void bubble_sort(char **ptr, size_t lines, bool desc, int word);
void printarray(char **ptr, size_t lines);
int isbigger(char *first, char *second, bool desc, int word);
int startofword(char *string, int word);

int main(int argc, char **argv)
{
    char line[MAXCHAR];
    char *ptr[MAXLINE];
    size_t lines = 0;
    bool desc = false;
    int word = 0;

	/* parse the command line arguments */
    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-d") == 0)
            desc = true;
        if (strcmp(argv[i], "-n") == 0)
            sscanf(argv[++i], "%d", &word);
    }
	
	/* read in the lines and store them in the pointer array*/
    while (fgets(line, MAXCHAR, stdin) != NULL) {
        ptr[lines] = strdup(line);
        lines++;
    }

    printarray(ptr, lines);
    printf("---\n");
    bubble_sort(ptr, lines, desc, word);
    printarray(ptr, lines);
}

/* print all the lines in the array */
void printarray(char **ptr, size_t lines) {
    for (int i = 0; i < lines; i++)
        printf("%s", ptr[i]);
}

/* sort the lines in the char array */
void bubble_sort(char **ptr, size_t lines, bool desc, int word)
{
	bool swapped = true;

	while (swapped) {
		swapped = false;
		for (int i = 0; i < lines-1; i++) {
			if (isbigger(ptr[i],ptr[i+1], desc, word)) {
                char *temp = ptr[i];
                ptr[i] = ptr[i+1];
                ptr[i+1] = temp;
                swapped = true;
			}
		}
	}
}

/* tests if the first string is 'bigger' */
int isbigger(char *first, char *second, bool desc, int word) 
{
	int firstbegin = startofword(first, word);
	int secondbegin = startofword(second, word);
    while (first[firstbegin] == second[secondbegin]) {
        ++firstbegin, ++secondbegin;
    }
    if (first[firstbegin] > second[secondbegin])
        return !desc;
    return desc;
}

/* get the start of the first word */
int startofword(char *string, int word) {
	if (word == 0)
		return word;
	for (int i = 0; i < strlen(string); i++) {
		if (string[i] == ' ')
			--word;
		if (word == 1)
			return i+1;
	}
	return strlen(string);
}
