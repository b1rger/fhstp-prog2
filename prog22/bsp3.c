#include <stdio.h>

int isPrime(int number);

int main(int argc, char** argv)
{
	int start, end, i, primes;
	printf("Enter a start number: ");
	scanf("%d", &start);
	printf("Enter an end number: ");
	scanf("%d", &end);

	for (i = start; i < end; i++) {
		if (isPrime(i)) {
			primes++;
			printf("%i ", i);
		}
	}
	float onepercent = (float)(end - start + 1) / 100;
	float percent = primes / onepercent;
	
	printf("\nSearch count %i (%i - %i), prim count %i, percentage %f\n", end - start + 1, end, start, primes, percent);
	
}

int isPrime(int number)
{
	int i;
	for (i = 2; i < number; i++) {
		if ((number % i == 0) & (i != number)) return 0;
	}
	return 1;
}
