#include <stdio.h>
#include <stdint.h>

int main(int argc, char **argv) {
	uint8_t i, e;

	printf("Bitte zwei Faktoren eingeben: ");

	if (scanf("%hhd %hhd", &i, &e) == 2) {
		if ((i > 10) | (e > 10))
			printf("Ihre Eingabe war nicht korrekt (nur Zahlen zwischen 1 und 10 erlaubt)!\n");

		for  (; i <= 10; i++) {
			for (uint8_t f = e; f <= 10; f++) {
				printf("%2i x %2i = %2i\n", i, f, i*f);
			}
		}
	} else {
		printf("Ihre Eingabe war nicht korrekt (nur Zahlen erlaubt)!\n");
	}
}
