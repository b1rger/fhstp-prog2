#include <stdio.h>

int add_special_type(long long both);

int main(int argc, char** argv) {
	signed int first, second;
	printf("Erste Zahl: ");
	scanf("%d", &first);
	printf("Zweite Zahl: ");
	scanf("%d", &second);

	long long both;
	both = first;
	both <<= 32;
	long temp = second;
	temp &= 0xFFFFFFFFL;
	both |= temp;
	printf("Abgespeichert als: %lld\n", both);

	printf("Addition ergibt: %d\n", add_special_type(both));
}

int add_special_type(long long both)
{
	long long first = both;
	first >>= 32;
	int second = (int)both;
	return first + second;
}
