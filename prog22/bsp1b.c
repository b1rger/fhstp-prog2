#include <stdio.h>
#include <stdint.h>

int main(int argc, char **argv) {
	uint8_t i = 1, e = 1;
	do {
		e = 1;
		do {
			printf("%2i x %2i = %2i\n", i, e, i*e);
			e++;
		} while (e <= 10);
		i++;
	} while (i <= 10);
}
