#include <stdio.h>
#include <stdint.h>

int main(int argc, char **argv) {
	uint8_t i, e;
	for (i = 1; i <= 10; i++) {
		for (e = 1; e <= 10; e++) {
			printf("%2i x %2i = %2i\n", i, e, i*e);
		}
	}
}
