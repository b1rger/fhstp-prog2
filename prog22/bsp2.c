#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>

uint8_t game(uint8_t p);
uint8_t newgame();
uint8_t getInput(uint8_t i);
uint8_t contains(uint8_t vals[], uint8_t val);

int main(int argc, char** argv)
{
	uint8_t p, rp = 0;

	printf("-- Willkommen zum Ratespiel --\n\n");
	while (!(p > 0 && p <= 4)) {
		printf("How many players (1-4)?\n");
		scanf("%hhi", &p);
	}
	rp = (p == 1) ? 2 : p;

	int players[4] = {0};

	players[game(p)]++;
	printf("Scores:\n");
	for (uint8_t i = 0; i < rp; i++) {
		printf("Player %i: %i\n", i+1, players[i]);
	}

	while (newgame() == 1) {
		players[game(p)]++;
		printf("Scores:\n");
		for (uint8_t i = 0; i < rp; i++) {
			printf("Player %i: %i\n", i+1, players[i]);
		}
	}
}

uint8_t newgame() {
	uint8_t c;
	printf("Neues Spiel (1) / Spiel beenden (2)\n");
	c = getchar();
	c = getchar();
	if ((c == 49) | (c == 10))
		return 1;
	return 2;
}

uint8_t game(uint8_t p) {
	uint8_t ki = 0, diff = 100, winner = 0;
	uint8_t players[p], val;

	if (p == 1)
		ki = 1;

	for (int i = 0; i < p; i++) {
		val = 0;
		while ((val == 0) | (contains(players, val))) {
			val = getInput(i);
		}
		players[i] = val;
	}

	if (ki) {
		srand(time(NULL));
		uint8_t r = rand() % 101;
		while (r == players[0]) {
			r = rand() % 101;
		}
		printf("Player 2 (KI) chooses: %i\n", r);
		players[1] = r;
		p++;
	}
	
	printf("----------\n");

	char n = rand() % 101;
	printf("The god of randomness chooses --> %i\n", n);

	for (int i = 0; i < p; i++) {
		if (abs(n - players[i]) < diff) {
			winner = i;
			diff = abs(n - players[i]);
		}
	}
	printf("The winner is player %i with a difference of %i\n", winner+1, diff);
	printf("----------\n");
	return winner;
}

uint8_t contains(uint8_t *vals, uint8_t val)
{
	for (int i = 0; i < 4; i++) {
		if (vals[i] == val)
			return 1;
	}
	return 0;
}

uint8_t getInput(uint8_t i)
{
	int c;
	printf("Number of player %i: ", i+1);
	scanf("%d", &c);
	return c;
}
