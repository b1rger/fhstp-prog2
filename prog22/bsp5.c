#include <stdio.h>
#include <stdint.h>

int pascal(uint8_t row, uint8_t col);

int main(int argc, char** argv)
{
	uint8_t rows;
	printf("Anzahl der Zeilen: ");
	scanf("%hhd", &rows);
	if ((rows < 1) | (rows > 20)) {
		printf("Es muessen zwischen 1 und 20 Zeilen sein.\n");
		return 1;
	}

	for (uint8_t i = 1; i <= rows; i++) {
		for (uint8_t s = 1; s < (rows-i)*2; s++)
			printf(" ");
		for (uint8_t e = 1; e <= i; e++) {
			printf("%i   ", pascal(i,e));
		}
		printf("\n");
	}
}

int pascal(uint8_t row, uint8_t col)
{
	if ((col == 0) | (row == 0))
		return 0;
	if ((row == 1) && (col == 1))
		return 1;

	return (pascal(row-1,col-1) + pascal(row-1,col));
}
